---
layout: 2020/post
section: propuestas
category: devrooms
title: Katas de programación para entrenamiento, con Haskell
---

Desde **HaskellMAD** planteamos un **hackathon** para la ejecución de _katas de programación para entrenamiento_. Se articula _íntegramente_ con herramientas de **software libre** conectadas con scripts de _bash_, para facilitar la flexibilidad en la elección de las piezas de software que se necesitan, así como su extensibilidad y mejora.

Agradeceríamos _propuestas de mejora_ o incluso de _alternativas_ de cualquier pieza de software, y tenemos interés en darles soporte y visibilidad desde el propio proyecto.

La elección de _Haskell_ como lenguaje para el desarrollo de las katas no es arbitrario. Se realizarán al menos _cuatro charlas_, una de ellas explicará el _porqué de la idoneidad de Haskell_ y de la _elección de software libre_ para estos menesteres, otra sobre el _proyecto de katas de programación de entrenamiento_ y otras dos sobre Haskell en general.

### Repositorio:
[katas-proof of concept](https://gitlab.com/HaskellKatas/katas--proof-of-concept/)


## Comunidad o grupo que lo propone

Las charlas y actividades estarán a cargo de miembros del meetup **HaskellMAD**, grupo que ya colaboró con [una charla](https://eslib.re/2019/programa/dev_web/haskell_hackers_codewars.html) en el anterior esLibre.

[HaskellMAD](https://www.meetup.com/Haskell-MAD/)

[Haskellnautas](https://haskellnautas.herokuapp.com/)

[Mozilla Day (UAH): Katas con Haskell para entrenamiento](https://www.meetup.com/Haskell-MAD/events/268851383/)

### Contactos

* Nombre: **Reynaldo Cordero Corro**: reynaldo.cordero @ uah.es

## Público objetivo

Dirigido a _cualquier asistente_. Se atenderá de forma muy especial a los _novatos_ en esta metodología, a los interesados en preparar el entorno de trabajo _desde cero_, a los que deseen resolver dudas de los _detalles de implementación e integración_ y a los que quieran _hacer aportaciones_ integrando sofware _alternativo_ o _modificando_ el existente o la _forma de usarlo_.

## Tiempo

Lo ideal sería hacer medio día el viernes y medio día el sábado.

## Día

Lo ideal sería hacer medio día el viernes y medio día el sábado.

## Formato

Hackathon con keynotes.

## Comentarios

Tenemos intención de aprovechar lo aprendido durante este esLibre para llevarlo al [ZuriHac 2020](https://zfoh.ch/zurihac2020/).

## Condiciones

* [X] Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [X] Al menos una persona entre los que proponen la devroom estará presente el día agendado para la *devroom*.
* [X] Acepto coordinarme con la organización de esLibre.
* [X] Entiendo que si no hay un programa terminado para la fecha que establezca la organización, la *devroom* podría retirarse.
