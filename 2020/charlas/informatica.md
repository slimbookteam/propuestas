---
layout: 2020/post
section: propuestas
category: talks
title: Enseñando informática en 2020&#58 una propuesta basada en el software libre
---

Es una propuesta de cambio de la enseñanza de la informática universitaria mediante un método que especifica más claramente qué es lo que se espera del código que pueda implementar el estudiante, asimilando los procesos de enseñanza/aprendizaje a procesos de integración/despliegue continuo. Y cómo lo he puesto en práctica en un par de asignaturas.

## Formato de la propuesta

-   [x] &nbsp;Charla (25 minutos)
-   [ ] &nbsp;Charla relámpago (10 minutos)

## Descripción

La informática cambia continuamente, y sin embargo la enseñanza de la misma, al menos en las universidades, sigue estando basada en clases magistrales, exámenes escritos (incluso de de prácticas), temarios y herramientas rígidas, aislamiento de asignaturas (incluso de la teoría y la práctica de la misma asignatura) asignatura y exámenes memorísticos.

Estos factores, y probablemente alguno más, provocan tasas de abandono altísimas, sobre todo en las fases iniciales y finales de la carrera; debidas a la baja motivación pero también a que se aprende más en cualquier otro lugar.

Pero el problema es que incluso entre las personas que se gradúan no se puede esperar un grado aceptable de conocimientos actualizados.

En esta charla lo que propongo es asemejar la enseñanza de la informática a un proceso de integración/despliegue continuo, con procesos automatizados de control de calidad, enseñanza-como-código y también cómo lo he puesto en práctica en un par de asignaturas (y un seminario) a base de scripts en Perl y TypeScript, Travis y GitHub Actions. Y cómo cualquiera que enseñe informática debería hacer lo mismo.

En este contexto, la propuesta se basa en uso de software libre por parte de los estudiantes y el profesor, liberación de todo el material, liberación como software libre de las entregas de los estudiantes, y es más, está con el espíritu del software libre porque se basa en la libertad del estudiante para elegir el proyecto que desee con el lenguaje y herramientas que desee.

## Público objetivo

Dirigida a cualquiera, a quien esté interesado en la parte técnica (cómo se ha implementado y cómo funcionan estos procesos), pero especialmente a profesionales de la enseñanza y a estudiantes que vean lo que se puede hacer en la enseñanza con un poco de ganas y lo que podrían exigir de sus profesores.

## Ponente(s)

**JJ Merelo Guervós**, profe en la UGR. No he dado esta charla nunca antes, pero sí la propuse en T3chFest y me la rechazaron. Sí he dado varias charlas sobre enseñanza, incluyendo una en las JENUI.

### Contacto(s)

-   **Juan Julián Merelo Guervós**: jjmerelo at gmail dot com | @JJ5

## Comentarios

25 minutos se queda un poco corto, pero por supuesto haré lo que la organización decida.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
